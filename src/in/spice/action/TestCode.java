package in.spice.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class TestCode {

	private class SheetToCSV implements SheetContentsHandler {
		private boolean firstCellOfRow = false;
		private int currentRow = -1;
		private int currentCol = -1;

		private void outputMissingRows(int number) {
			if ((maxRows == -1) || (maxRows != -1 && currentRow < maxRows)) {
				for (int i = 0; i < number; i++) {
					for (int j = 0; j < minColumns; j++) {
						output.append(separator);
					}
					output.append('\n');
				}
			}
		}

		public void startRow(int rowNum) {
			if ((maxRows == -1) || (maxRows != -1 && currentRow < maxRows)) {
				// If there were gaps, output the missing rows
				outputMissingRows(rowNum - currentRow - 1);
				// Prepare for this row
				firstCellOfRow = true;
				currentRow = rowNum;
				currentCol = -1;
			}
		}

		public void endRow(int rowNum) {
			if ((maxRows == -1) || (maxRows != -1 && currentRow < maxRows)) {
				// Ensure the minimum number of columns
				for (int i = currentCol; i < minColumns; i++) {
					output.append(separator);
				}
				output.append('\n');
			}
		}

		public void cell(String cellReference, String formattedValue, XSSFComment comment) {
			if ((maxRows == -1) || (maxRows != -1 && currentRow < maxRows)) {
				if (firstCellOfRow) {
					firstCellOfRow = false;
				} else {
					output.append(separator);
				}

				// gracefully handle missing CellRef here in a similar way as
				// XSSFCell does
				if (cellReference == null) {
					cellReference = new CellAddress(currentRow, currentCol).formatAsString();
				}

				// Did we miss any cells?
				int thisCol = (new CellReference(cellReference)).getCol();
				int missedCols = thisCol - currentCol - 1;
				for (int i = 0; i < missedCols; i++) {
					output.append(separator);
				}
				currentCol = thisCol;

				// Number or string?
				try {
					Double.parseDouble(formattedValue);
					output.append(formattedValue);
				} catch (NumberFormatException e) {
					// output.append('"');
					output.append(formattedValue);
					// output.append('"');
				}
			}
		}

		public void headerFooter(String text, boolean isHeader, String tagName) {
			// Skip, no headers or footers in CSV
		}
	}

	private final OPCPackage xlsxPackage;

	/**
	 * Number of columns to read starting with leftmost
	 */
	private final int minColumns;
	private final int maxRows;
	private final String separator;

	/**
	 * Destination for data
	 */
	private final StringBuilder output;

	/**
	 * Creates a new XLSX -> CSV converter
	 *
	 * @param pkg
	 *            The XLSX package to process
	 * @param output
	 *            The PrintStream to output the CSV to
	 * @param minColumns
	 *            The minimum number of columns to output, or -1 for no minimum
	 */
	public TestCode(OPCPackage pkg, StringBuilder output, int minColumns, String separator, int maxRows) {
		this.xlsxPackage = pkg;
		this.output = output;
		this.minColumns = minColumns;
		this.separator = separator;
		this.maxRows = maxRows;
	}

	/**
	 * Parses and shows the content of one sheet using the specified styles and
	 * shared-strings tables.
	 *
	 * @param styles
	 * @param strings
	 * @param sheetInputStream
	 */
	public void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, SheetContentsHandler sheetHandler,
			InputStream sheetInputStream) throws IOException, ParserConfigurationException, SAXException {
		DataFormatter formatter = new DataFormatter();
		InputSource sheetSource = new InputSource(sheetInputStream);
		try {
			XMLReader sheetParser = SAXHelper.newXMLReader();
			ContentHandler handler = new XSSFSheetXMLHandler(styles, null, strings, sheetHandler, formatter, false);
			sheetParser.setContentHandler(handler);
			sheetParser.parse(sheetSource);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
		} finally {
			try {
				if (sheetInputStream != null) {
					sheetInputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void writeToFile(String resultFilePath) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(resultFilePath);
			// System.out.println(resultFilePath + " a a " + output.length());
			fos.write(output.toString().getBytes());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Initiates the processing of the XLS workbook file to CSV.
	 *
	 * @throws IOException
	 * @throws OpenXML4JException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public void process(String resultFilePath)
			throws IOException, OpenXML4JException, ParserConfigurationException, SAXException {
		ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.xlsxPackage);
		XSSFReader xssfReader = new XSSFReader(this.xlsxPackage);
		StylesTable styles = xssfReader.getStylesTable();
		XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
	
		if (iter.hasNext()) {
			InputStream stream = iter.next();
			processSheet(styles, strings, new SheetToCSV(), stream);
			stream.close();
		}
	}

	public String getHeaderString() {
		if (output != null) {
			return output.toString();
		} else {
			return null;
		}
	}

	public static void main(String[] args) throws Exception {

		// String filepath = "F:/DotFiles/bkp/test.xlsx";
		// File xlsxFile = new File(filepath);
		// if (!xlsxFile.exists()) {
		// System.err.println("Not found or not a file: " + xlsxFile.getPath());
		// return;
		// }
		// int minColumns = -1;
		// long start = System.currentTimeMillis();
		// System.out.println("Start At: " + start);
		// StringBuilder sb = new StringBuilder();
		// OPCPackage p = OPCPackage.open(xlsxFile.getPath(),
		// PackageAccess.READ);
		// TestCode xlsx2csv = new TestCode(p, sb, minColumns, ",", -1);
		// System.out.println("Start Processing At: " +
		// (System.currentTimeMillis() - start));
		// xlsx2csv.process("F:/DotFiles/bkp/hello.txt");
		// System.out.println("End Processing At: " +
		// (System.currentTimeMillis() - start));
		// System.out.println("End Processing At: " +
		// xlsx2csv.getHeaderString());
		// xlsx2csv.writeToFile("F:/DotFiles/bkp/hello.txt");
		// System.out.println("FinalFileResult Processing At: " +
		// (System.currentTimeMillis() - start));
		// p.close();
	}
}