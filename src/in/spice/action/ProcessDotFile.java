package in.spice.action;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import com.monitorjbl.xlsx.StreamingReader;

import in.spice.db.action.DBProcessDotFile;
import in.spice.pojo.ExcelFileDetailPojo;
import in.spice.util.Log;
import in.spice.util.LogStackTrace;
import in.spice.util.OsType;
import in.spice.util.Utilities;

public class ProcessDotFile {

	public static void main(String[] args) {

		long startTime = System.currentTimeMillis();

		ProcessDotFile objProcessDotFile = new ProcessDotFile();
		String file_name = "";
		try {
			File dir = null;
			if (OsType.isWindows()) {
				dir = new File(Utilities.getConfKeyValue("windowfile.location"));
			} else {
				dir = new File(Utilities.getConfKeyValue("linuxfile.location"));
			}
			File[] dirListing = dir.listFiles();
			System.out.println("list of files in dir: " + dirListing.length);
			Log.getAccessLog("Total Number of excel file: " + dirListing.length);
			for (int i = 0; i < dirListing.length; i++) {
				if (!dirListing[i].isDirectory()) {
					if ("xls".equalsIgnoreCase(Utilities.getFileExtension(dirListing[i].getName()))) {
						file_name = dirListing[i].getName();
						objProcessDotFile.readXLSFile(dirListing[i]);

					} else if ("xlsx".equalsIgnoreCase(Utilities.getFileExtension(dirListing[i].getName())))
						file_name = dirListing[i].getName();
					objProcessDotFile.readXLSXFile(dirListing[i]);

				}
			}
			System.out.println("Process All File Successfully.....");
			long endTime = System.currentTimeMillis();
			System.out.println("Total Time: " + (endTime - startTime));
		} catch (Exception e) {
			Log.getErrorLog("file_name: " + file_name + "   " + LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	private void readXLSXFile(File processFile) throws Exception {

		// System.out.println("enter1..............");
		Workbook workbook = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(processFile);
		// System.out.println("enter2...............");
		int numberOfSheet = workbook.getNumberOfSheets();
		// System.out.println("number of sheets.........." + numberOfSheet);
		for (int j = 0; j < numberOfSheet; j++) {
			List<ExcelFileDetailPojo> list = new ArrayList<ExcelFileDetailPojo>();
			// Sheet sheet = (Sheet) workbook.getSheetAt(j);
			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(j);
			// System.out.println("File Name: " + processFile.getName() + "
			// ,Sheet Name: " + sheet.getSheetName());
			Log.getAccessLog("File Name: " + processFile.getName() + " , Sheet Name: " + sheet.getSheetName());
			Iterator<Row> rowIterator = sheet.iterator();
			Map<String, Integer> cellMap = new HashMap<String, Integer>();
			invalidSheet: while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (row.getRowNum() == 0) {
					Iterator<Cell> cellIterator = row.cellIterator();
					while (cellIterator.hasNext()) {
						// System.out.println("hellooo1");
						Cell cell = cellIterator.next();
//						if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){
//							switch(cell.getCachedFormulaResultType()){
//							case Cell.CELL_TYPE_NUMERIC:
//								System.out.println(cell.getNumericCellValue());
//								break;
//							case Cell.CELL_TYPE_STRING:
//								System.out.println(cell.getRichStringCellValue());
//								break;
//							}
//						}
						cellMap.put(cell.getStringCellValue().trim().toUpperCase(), cell.getColumnIndex());
					}
				} else {
					if (cellMap.containsKey("MOBILE") && cellMap.containsKey("OPERATOR")
							&& cellMap.containsKey("LSA")) {

						Cell mobCell = row.getCell(cellMap.get("MOBILE"));
						Cell operatorCell = row.getCell(cellMap.get("OPERATOR"));
						Cell circleCell = row.getCell(cellMap.get("LSA"));
						Cell cityCell = null;
						Cell postalCodeCell = null;
						if (cellMap.containsKey("CITY")) {
							cityCell = row.getCell(cellMap.get("CITY"));
						}
						if (cellMap.containsKey("POSTAL CODE")) {
							postalCodeCell = row.getCell(cellMap.get("POSTAL CODE"));
						}
						if ((mobCell != null && !"".equals(mobCell))
								&& (operatorCell != null && !"".equals(operatorCell)) && circleCell != null
								&& !"".equals(circleCell)) {
							String mobileNumber = getValueFromCell(mobCell);
							if (isNumeric(mobileNumber) && mobileNumberCheck(mobileNumber)) {
								String operator = getValueFromCell(operatorCell);
								String circle = getValueFromCell(circleCell);
								String city = null;
								String postalCode = null;
								if (cityCell != null && !"".equals(cityCell)) {
									city = getValueFromCell(cityCell);
								}
								if (postalCodeCell != null && !"".equals(postalCodeCell)) {
									postalCode = getValueFromCell(postalCodeCell);
								}
								if ((mobileNumber != null && !"".equals(mobileNumber))
										&& (operator != null && !"".equals(operator))
										&& (circle != null && !"".equals(circle))) {
									ExcelFileDetailPojo obj = new ExcelFileDetailPojo();
									obj.setCircle(circle.trim());
									obj.setMobile(mobileNumber.trim());
									obj.setOperator(operator.trim());
									if (city != null && !"".equals(city)) {
									     obj.setCity(city.trim());
									}else{
										obj.setCity(null);
									}
									if (postalCode != null && !"".equals(postalCode)) {
									obj.setPostalCode(postalCode.trim());
									}else{
										obj.setPostalCode(null);
									}
									obj.setFileName(processFile.getName());
									list.add(obj);
								}
							}
						}
					} else {
						Log.getAccessLog("File Name: " + processFile.getName() + " , Sheet Name: "
								+ sheet.getSheetName() + " , Record Size: " + list.size() + " , invalid sheet");
						break invalidSheet;
					}
				}
			}
			Log.getAccessLog("File Name: " + processFile.getName() + " ,Sheet Name: " + sheet.getSheetName()
					+ " , Record Size: " + list.size());
			new DBProcessDotFile().insertData(list);
			// System.out.println("list Size: " + list.size());
			list.clear();
		}

	}

	private void readXLSFile(File processFile) {
		try {
			// System.out.println("enter...................");
			FileInputStream file = new FileInputStream(processFile);
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			// System.out.println("enter2...................");
			int numberOfSheet = workbook.getNumberOfSheets();
			for (int j = 0; j < numberOfSheet; j++) {
				List<ExcelFileDetailPojo> list = new ArrayList<ExcelFileDetailPojo>();
				HSSFSheet sheet = workbook.getSheetAt(j);
				// System.out.println("Sheet Name: " + sheet.getSheetName());
				Log.getAccessLog("File Name: " + processFile.getName() + " , Sheet Name: " + sheet.getSheetName());
				Iterator<Row> rowIterator = sheet.iterator();
				Map<String, Integer> cellMap = new HashMap<String, Integer>();
				sheetTest: while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					if (row.getRowNum() == 0) {
						Iterator<Cell> cellIterator = row.cellIterator();
						while (cellIterator.hasNext()) {
							Cell cell = cellIterator.next();
							
							cellMap.put(cell.getStringCellValue().trim().toUpperCase(), cell.getColumnIndex());
						}
					} else {

						if (cellMap.containsKey("MOBILE") && cellMap.containsKey("OPERATOR")
								&& cellMap.containsKey("LSA")) {

							Cell mobCell = row.getCell(cellMap.get("MOBILE"));
							Cell operatorCell = row.getCell(cellMap.get("OPERATOR"));
							Cell circleCell = row.getCell(cellMap.get("LSA"));
							Cell cityCell = null;
							Cell postalCodeCell = null;
							if (cellMap.containsKey("CITY")) {
								cityCell = row.getCell(cellMap.get("CITY"));
							}
							if (cellMap.containsKey("POSTAL CODE")) {
								postalCodeCell = row.getCell(cellMap.get("POSTAL CODE"));
							}
							if ((mobCell != null && !"".equals(mobCell))
									&& (operatorCell != null && !"".equals(operatorCell)) && circleCell != null
									&& !"".equals(circleCell)) {
								String mobileNumber = getValueFromCell(mobCell);
								if (isNumeric(mobileNumber) && mobileNumberCheck(mobileNumber)) {
									String operator = getValueFromCell(operatorCell);
									String circle = getValueFromCell(circleCell);
									String city = null;
									String postalCode = null;
									if (cityCell != null && !"".equals(cityCell)) {
										city = getValueFromCell(cityCell);
									}
									if (postalCodeCell != null && !"".equals(postalCodeCell)) {
										postalCode = getValueFromCell(postalCodeCell);
									}
									if ((mobileNumber != null && !"".equals(mobileNumber))
											&& (operator != null && !"".equals(operator))
											&& (circle != null && !"".equals(circle))) {
										ExcelFileDetailPojo obj = new ExcelFileDetailPojo();
										obj.setCircle(circle.trim());
										obj.setMobile(mobileNumber.trim());
										System.out.println(mobileNumber.trim());
										obj.setOperator(operator.trim());
										if (city != null && !"".equals(city)) {
										     obj.setCity(city.trim());
										}else{
											obj.setCity(null);
										}
										if (postalCode != null && !"".equals(postalCode)) {
											obj.setPostalCode(postalCode.trim());
											}else{
												obj.setPostalCode(null);
											}
										obj.setFileName(processFile.getName());
										list.add(obj);
									}
								}
							}
						} else {
							Log.getAccessLog("File Name: " + processFile.getName() + " , Sheet Name: "
									+ sheet.getSheetName() + " , Record Size: " + list.size() + " , invalid sheet");
							break sheetTest;
						}
					}
				}
				Log.getAccessLog("File Name: " + processFile.getName() + " , Sheet Name: " + sheet.getSheetName()
						+ " , Record Size: " + list.size());
				new DBProcessDotFile().insertData(list);
				// System.out.println("list Size: " + list.size());
				list.clear();
			}
		} catch (Exception e) {
			Log.getErrorLog(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}

		// System.out.println("enter1..............");
		// Workbook workbook =
		// StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(processFile);
		// System.out.println("enter2...............");
		// int numberOfSheet = workbook.getNumberOfSheets();
		// System.out.println("number of sheets.........." + numberOfSheet);
		// for (int j = 0; j < numberOfSheet; j++) {
		// List<ExcelFileDetailPojo> list = new
		// ArrayList<ExcelFileDetailPojo>();
		// // Sheet sheet = (Sheet) workbook.getSheetAt(j);
		//
		// org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(j);
		//
		// System.out.println("File Name: " + processFile.getName() + " ,Sheet
		// Name: " + sheet.getSheetName());
		// Log.getAccessLog("File Name: " + processFile.getName() + " , Sheet
		// Name: " + sheet.getSheetName());
		// Iterator<Row> rowIterator = sheet.iterator();
		// Map<String, Integer> cellMap = new HashMap<>();
		// while (rowIterator.hasNext()) {
		// Row row = rowIterator.next();
		// if (row.getRowNum() == 0) {
		// Iterator<Cell> cellIterator = row.cellIterator();
		// while (cellIterator.hasNext()) {
		// // System.out.println("hellooo1");
		// Cell cell = cellIterator.next();
		// cellMap.put(cell.getStringCellValue().trim().toUpperCase(),
		// cell.getColumnIndex());
		// }
		// } else {
		// Cell mobCell = row.getCell(cellMap.get("MOBILE"));
		// String mobileNumber = getValueFromCell(mobCell);
		// Cell operatorCell = row.getCell(cellMap.get("OPERATOR"));
		// String operator = getValueFromCell(operatorCell);
		// Cell circleCell = row.getCell(cellMap.get("LSA"));
		// String circle = getValueFromCell(circleCell);
		// ExcelFileDetailPojo obj = new ExcelFileDetailPojo();
		// obj.setCircle(circle.trim());
		// obj.setMobile(mobileNumber.trim());
		// obj.setOperator(operator.trim());
		// list.add(obj);
		// }
		// }
		// Log.getAccessLog("File Name: " + processFile.getName() + " ,Sheet
		// Name: " + sheet.getSheetName()
		// + " , Record Size: " + list.size());
		// new DBProcessDotFile().insertData(list);
		// System.out.println("list Size: " + list.size());
		// list.clear();
		// }

	}

	private String getValueFromCell(Cell cell) {
		String strGetValue = "";
//		 if (cell.getStringCellValue().toString() == null) {
//			
//			 }   else{
//		System.out.println(valueOf(cell.getStringCellValue().toString()));
//			 }
//		if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){
//			 
//			switch(cell.getCachedFormulaResultType()){
//			case Cell.CELL_TYPE_NUMERIC:
//				System.out.println(cell.getNumericCellValue());
//				break;
//			case Cell.CELL_TYPE_STRING:
//				System.out.println(cell.getRichStringCellValue());
//				break;
//			}
//		}
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			strGetValue = cell.getBooleanCellValue() + "";
			break;
		case Cell.CELL_TYPE_NUMERIC:
			strGetValue = new BigDecimal(cell.getNumericCellValue()).toPlainString();
			if (DateUtil.isCellDateFormatted(cell)) {
				strGetValue = new DataFormatter().formatCellValue(cell);
			} else {
				strGetValue = new BigDecimal(cell.getNumericCellValue()).toPlainString();
			}
			String tempStrGetValue = removeSpace(strGetValue);
			if (tempStrGetValue.length() == 0) {
				strGetValue = "";
			} else {
				strGetValue = strGetValue + "";
			}
			break;
		case Cell.CELL_TYPE_STRING:
			strGetValue = cell.getStringCellValue();
			String tempStrGetValue1 = removeSpace(strGetValue);
			if (tempStrGetValue1.length() == 0) {
				strGetValue = " ";
			} else {
				strGetValue = strGetValue + "";
			}
			break;
		case Cell.CELL_TYPE_BLANK:
			strGetValue = "";
			break;
		case Cell.CELL_TYPE_FORMULA:
			strGetValue = valueOf(cell.getStringCellValue().toString()) ;
			break;
		default:
			strGetValue = cell + "";
		}
		return strGetValue;
	}

	private String valueOf(String stringCellValue) {
		// TODO Auto-generated method stub
		return stringCellValue.replaceAll("\"", "");
	}

	private static String removeSpace(String strString) {
		if (strString != null && !strString.equals("")) {
			return strString.trim();
		}
		return strString;
	}

	private boolean isNumeric(String s) {
		return s.matches("[-+]?\\d*\\.?\\d+");
	}

	private boolean mobileNumberCheck(String mobileNumber) {
		String key = "(0|91)?[789][0-9]{9}";
		boolean mobileFlag = mobileNumber.matches(key);
		return mobileFlag;
	}

}
