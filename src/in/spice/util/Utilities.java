package in.spice.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utilities {
	private static Properties objProperties;
    public static String getConfKeyValue(String key) throws IOException {
           FileInputStream fis = null;
           try {
                  String filePath = "";
                  if (objProperties == null) {
                        objProperties = new Properties();
                        if (OsType.isWindows()) {
                               filePath = "D:/dot.properties";
                        } else {
                               filePath = "/home/DOT_OBD_Process/dot.properties";
                        }
                        fis = new FileInputStream(filePath);
                        objProperties.load(fis);
                  }
           } catch (Exception e) {
                  System.out.println("Error while loading Property file:" + e.getMessage());
           } finally {
                  if (fis != null) {
                        fis.close();
                  }
           }
           return objProperties.getProperty(key);
    }
    
    public static void reload()
    {
    	objProperties=null;
    }
    public static String getFileExtension(String fileName) {
		String extension = "";
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		return extension;
	}
    
    
}