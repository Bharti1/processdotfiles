package in.spice.util;

import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

public class Log {

	private static synchronized void writeLog(String logString, String strSubDir) {

		String strFileName = "";
		String strDateDir = "";
		try {
			if (OsType.isWindows()) {
				//strDateDir = "G:/DOT_OBD_Process/ProcessDotFileLogs/";
				strDateDir = "D:/ProcessDotFileLogs/";
			} else {
				strDateDir = "/home/DOT_OBD_Process/logs/";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		Calendar objCalendarRightNow = Calendar.getInstance();
		int intMonth = objCalendarRightNow.get(Calendar.MONTH) + 1;
		int intDate = objCalendarRightNow.get(Calendar.DATE);
		int intHour = objCalendarRightNow.get(Calendar.HOUR_OF_DAY);
		int intMinute = objCalendarRightNow.get(Calendar.MINUTE);
		int intSecond = objCalendarRightNow.get(Calendar.SECOND);
		int intMilliSecond = objCalendarRightNow.get(Calendar.MILLISECOND);
		String strYear = "" + objCalendarRightNow.get(Calendar.YEAR);
		System.out.println(strDateDir);
		strDateDir = strDateDir + "" + intDate + "-" + intMonth + "-" + strYear;
		createDateDir(strDateDir);
		strFileName = "" + strDateDir + "/" + strSubDir + ".log";
		try {
			FileWriter out = new FileWriter(strFileName, true);
			String strLogString = intHour + ":" + intMinute + ":" + intSecond + ":" + intMilliSecond + "," + logString
					+ "\n";
			out.write(strLogString);
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(0);
		}
	}

	private static synchronized void createDateDir(String dateDir) {
		try {
			new File(dateDir).mkdirs();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static synchronized void getAccessLog(String Trace) {
		writeLog(Trace, "AccessLogs");
	}

	public static synchronized void getDatabaseLog(String Database) {
		writeLog(Database, "Database");
	}

	public static synchronized void getErrorLog(String Error) {
		writeLog(Error, "Error");
	}

}
