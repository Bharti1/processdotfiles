package in.spice.db;

import java.sql.Connection;
import java.sql.DriverManager;

import in.spice.util.Utilities;

public class DBManager {
	public Connection getConnection() {
		Connection objConnection = null;
		try {
			Class.forName(Utilities.getConfKeyValue("database.driver"));
			objConnection=DriverManager.getConnection(Utilities.getConfKeyValue("database.url"),Utilities.getConfKeyValue("database.username"), Utilities.getConfKeyValue("database.password"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objConnection;
	}

}
