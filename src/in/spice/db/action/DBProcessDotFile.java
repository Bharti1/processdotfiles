package in.spice.db.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import in.spice.db.DBManager;
import in.spice.pojo.ExcelFileDetailPojo;
import in.spice.util.Log;
import in.spice.util.LogStackTrace;

public class DBProcessDotFile {

	public void insertData(List<ExcelFileDetailPojo> list) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int batchSize = 999;
		int counter = 0;
		try {
			DBManager objDBManager = new DBManager();
			String query = "insert into dot_obd_dump(msisdn,operator,lsa,month,lang,obd_status,city,postal_code,term_cell) values(?,?,?,?,?,?,?,?,?)";
			con = objDBManager.getConnection();
			pStmt = con.prepareStatement(query);
			for (int i = 0; i < list.size(); i++) {
				pStmt.setString(1, list.get(i).getMobile());
				pStmt.setString(2, list.get(i).getOperator());
				pStmt.setString(3, list.get(i).getCircle());
				pStmt.setString(4, getMonthYear());
				pStmt.setString(5, "h");
				pStmt.setString(6, "0");
				pStmt.setString(7, list.get(i).getCity());
				pStmt.setString(8, list.get(i).getPostalCode());
				pStmt.setString(9, list.get(i).getFileName());
				pStmt.addBatch();
				if (++counter % batchSize == 0) {
					pStmt.executeBatch();
					System.out.println("Batch uploaded.....");
					pStmt.clearParameters();
				}
			}
			pStmt.executeBatch();
			pStmt.clearParameters();
		} catch (Exception e) {
			Log.getDatabaseLog(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(pStmt);
		}
	}
	
	private String getMonthYear()
	{
		Calendar cal = Calendar.getInstance();
		String month = new SimpleDateFormat("MMM").format(cal.getTime());
		int year = cal.get(Calendar.YEAR);
		String month_year = month+" "+year;
	//	System.out.println(month_year);
		
		return month_year;
	}

}
